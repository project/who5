<?php
/**
* @file
*   The content of the About page, read as a long block into the $raw_content variable   
*/
$raw_content = <<<EOD
<blockquote>
<div style="margin-bottom:10px;">
<h1>Background</h1>
<p>
The Psychiatric Research Unit, Frederiksborg General Hospital, Hillerød, Denmark, is a WHO Collaborating Centre for Mental Health.
</p>
<p>
Over the years, the WHO Collaborating Center at Frederiksborg General Hospital in Hillerød, Denmark, has worked with the concept of subjective quality of life as a dimension separate from social disability (Bech 1993). The core items of subjective quality of life belong to the dimension of psychological well-being (positive mood, vitality and interest in things). In contrast to this, social disability includes the restriction of the ability to perform daily activities.
</p>
<p>
The WHO-Five Well-being Index was derived from a larger rating scale developed for a WHO project on quality of life in patients suffering from diabetes (WHO 1990). During the first psychometric evaluation, 10 of the original 28 items were selected due to the homogeneity they had shown across the various European countries participating in this study (Bech et al 1996).
</p>
<p>
Because positive psychological well-being has to include positively worded items only, these 10 items were then reduced to five items (WHO-Five) which still covered positive mood (good spirits, relaxation), vitality (being active and waking up fresh and rested), and general interests (being interested in things) (Bech 1998, 2001).
</p>
<h3>References</h3>
<p>
Bech P, Gudex C, Staehr Johansen K. The WHO (Ten) Well-Being Index: Validation in Diabetes. Psychother Psychosom 1996; 65: 183-190.<br />
</p>
<p>
Bech P. Rating scales for psychopathology, health status and quality of life. A compendium on documentation in accordance with the DSM-III-R and WHO systems. Springer: Berlin 1993.<br />
</p>
<p>
Bech P. Quality of life in the psychiatric patient. London: Mosby-Wolfe 1998.<br />
</p>
<p>
Bech P. Male depression: stress and aggression as pathways to major depression. In: Dawson A and Tylee A (eds.) Depression: Social and economic timebomb. London: BMJ Books 2001, pp 63-66.<br />
</p>
<p>
World Health Organization Regional Office for Europe and the International Diabetes Federation, Europe. Diabetes mellitus in Europe: a problem at all ages and in all countries. A model for prevention and self care. Meeting. Giorn Ital Diabetol 1990; 10 (suppl).<br />
</p>
</div>
<div>
<h1>Interpretation of the Items of the WHO-5 Questionnaire</h1>
<p>
<h3>Rating</h3>
Each of the five items is rated on a 6-point Likert scale from 0 (= not present) to 5 (= constantly present). The theoretical raw score ranges from 0 to 25 and is transformed into a scale from 0 (worst thinkable well-being) to 100 (best thinkable well-being). Thus, higher scores mean better well-being.
</p>
<p>
<h3>Interpretation</h3>
The raw score is obtained by adding the figures in the boxes. The score range is from 0 to 25. It is recommended to administer the Major Depression (ICD-10) Inventory if the raw score is below 13 or if the patient has answered 0 to 1 to any of the five items. A score below 13 indicates poor well-being and is an indication for testing for depression under ICD-10.
</p>
<p>
<h3>Monitoring change</h3>
In order to monitor possible changes in well-being, the percentage score is used. The percentage value is obtained by multiplying the score by 4. A 10% difference indicates a significant change.
</p>

</div>
</blockquote>
EOD;
