<?php
/**
* @file
*   Defines WHO5Report class
*
*
*/
class WHO5Report {
  //static attributes
  //can't be constants (which conceptually they are) because they're arrays
  public static $Question_Array = array(
    'I have felt cheerful and in good spirits',
    'I have felt calm and relaxed',
    'I have felt active and vigorous',
    'I woke up feeling fresh and rested',
    'My daily life has been filled with things that interest me'
    );  
   
  public static $Answer_Array = array(
    5 => 'All of the time',
    4 => 'Most of the time',
    3 => 'More than half of the time',
    2 => 'Less than half of the time',
    1 => 'Some of the time',
    0 => 'At no time'
    );

  //attributes passed to constructor
  private $score_array;
  private $assessment_date;

  //calculated attributes
  public $total_score;
  public $pct_score;
  public $assessment_date_string;
  public $failed_questions = array();  //questions whose individual response is 0 or 1
  public $failed_question_count;  
  
  /**
  *  Constructor
  *
  * @param $score_array
  *    Array of individual item scores from the WHO5 form
  * @param $assessment_date
  *    A unix timestamp fo the datetime at which the assessment was completed
  */
  public function __construct($score_array, $assessment_date) {
    $this->score_array = $score_array;
    $this->assessment_date = $assessment_date;

    //initialize calculated fields
    $this->total_score = array_sum($score_array);
    $this->pct_score = $this->total_score * 4;
    $this->assessment_date_string = date('m-d-Y h:i:s', $assessment_date);
    $this->failed_questions = $this->_get_failed_questions();
    $this->failed_question_count = count($this->failed_questions);
 }

  /**
  *  Return array of the 'failed' questions and their scores
  *
  *  @return
  *	array $failed_questions
  */
  private function _get_failed_questions() {
    //check individual scores for problems (i.e., any score of 0 or 1)
    $failed_questions = array();
    foreach ($this->score_array as $i => $score) {
      if ($score <= 1) //any individual score of 1 or 0 represents a warning sign
        $failed_questions[$i] = $score;
    }
    return $failed_questions;
  }  
}
